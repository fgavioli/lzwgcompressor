# LZWGCompressor - Lempel-Ziv-Welch-Gavioli Compressor

Gavioli Federico's python implementation of the dictionary-based Lempel-Ziv-Welch compressor.

Setup
python3 setup.py

Usage
./compress.py [filename]
./decompress.py [filename]
